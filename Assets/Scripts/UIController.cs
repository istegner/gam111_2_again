﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{

    public GameObject[] attackButtons;
    public GameObject[] monsterButtons;
    public GameObject[] monsters;
    public GameObject gameManager;

    public GameObject currentMonster;

    public bool playerTurn;

    // Use this for initialization
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController");
        if(SceneManager.GetActiveScene().name == "Battle")
        {
            UpdateAttackButtonNames();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(SceneManager.GetActiveScene().name == "Battle" && attackButtons.Length < 1)
        {
            GameObject[] buttons = GameObject.FindGameObjectsWithTag("AbilityButton");
            for(int i = 0; i < buttons.Length; i++)
            {
                attackButtons[i] = buttons[i];
            }
            buttons = GameObject.FindGameObjectsWithTag("MonsterButton");
            for (int i = 0; i < buttons.Length; i++)
            {
                monsterButtons[i] = buttons[i];
            }
        }
        if (playerTurn)
        {

        }
    }
    
    public void UpdateAttackButtonNames()
    {
        for (int i = 0; i < attackButtons.Length; i++)
        {
            var txt = attackButtons[i].GetComponentInChildren<Text>();
            if (txt != null)
            {
                if (i >= gameManager.GetComponent<GameManager>().player.GetComponent<PlayerController>().abilities.Length)
                {
                    txt.text = gameManager.GetComponent<GameManager>().player.GetComponent<PlayerController>().abilities[i].name;
                }
                else
                {
                    attackButtons[i].SetActive(false);
                }
            }
        }
    }

    public void SetMonster(int monsterInt)
    {
        currentMonster = monsters[monsterInt];
    }
}
