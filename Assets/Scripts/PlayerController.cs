﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public int longGrassChance;
    public GoToBattle goToBattle;
    public Ability[] abilities;
    public GameObject gameManager;
    public int timeToFight;

    private void Start()
    {
        timeToFight = Random.Range(10, 1000);
        DontDestroyOnLoad(this.gameManager);
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "LongGrass" && timeToFight == 0)
        {
            goToBattle.player = this.gameObject;
            goToBattle.npc = gameManager.GetComponent<GameManager>().NPCs[Random.Range(0, gameManager.GetComponent<GameManager>().NPCs.Length)];
            goToBattle.BattleTime();
        }
        else if (other.gameObject.tag == "LongGrass")
        {
            Debug.Log(timeToFight);
            timeToFight--;
        }
    }
}
