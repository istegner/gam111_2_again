﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class BattleContext : ScriptableObject
{
    public GameObject[] enemies;
    public GameObject enemy;
    public int enemyState;

    private void OnEnable()
    {
        enemyState = 0;
    }

    void Start()
    {
        if (enemy == null)
        {
            enemy = enemies[Random.Range(0, enemies.Length)];
        }
    }
}
