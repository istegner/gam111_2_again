﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu()]
public class GoToBattle : ScriptableObject
{
    public string battleSceneName;
    public GameObject player;
    public NPC npc;

    public GameObject[] objectsToDeactivate;

    public void BattleTime()
    {
        SceneManager.LoadScene(battleSceneName);
        for(int i = 0; i < objectsToDeactivate.Length; i++)
        {
            objectsToDeactivate[i].SetActive(false);
        }
    }

    public void BattleEnd()
    {
        if (SceneManager.sceneCount > 1)
        {
            SceneManager.UnloadSceneAsync(battleSceneName);
            for (int i = 0; i < objectsToDeactivate.Length; i++)
            {
                objectsToDeactivate[i].SetActive(true);
            }
        }
    }
}
