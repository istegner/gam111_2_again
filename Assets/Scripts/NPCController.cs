﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCController : MonoBehaviour
{
    [Header("Idle stuff")]
    public Transform[] waypoints;
    NavMeshAgent agent;
    public float stoppingDistance;
    public NPC npc;

    [Header("Player stuff")]
    public Transform player;
    public float seeDistance;
    bool canSeePlayer;

    [Header("Battle stuff")]
    public GoToBattle goToBattle;

    /// <summary>
    /// Get the NPC's navmesh agent component and set it on it's way
    /// </summary>
    void Start()
    {
        agent = gameObject.GetComponent<NavMeshAgent>();
        agent.SetDestination(waypoints[Random.Range(0, waypoints.Length)].position);
        DontDestroyOnLoad(this.gameObject);
    }

    /// <summary>
    /// Check if the NPC can see the player
    /// If the NPC is close enough and can see the player, chase the player and start a fight
    /// If the NPC cannot see the player or is too far away, go to a random waypoint
    /// </summary>
    void Update()
    {
        if (npc.state == 1)
        {
            //Check if the NPC can see the player
            RaycastHit hit;
            var rayDirection = player.position - transform.position;
            if (Physics.Raycast(transform.position, rayDirection, out hit))
            {
                Debug.DrawRay(transform.position, rayDirection);
                if (hit.transform.gameObject.CompareTag("Player"))
                {
                    canSeePlayer = true;
                    //Debug.Log("Can see the player");
                }
                else
                {
                    canSeePlayer = false;
                    //Debug.Log("Can't see the player");
                }
            }
            //Debug.Log(Vector3.Distance(transform.position, player.position));

            if (canSeePlayer && Vector3.Distance(transform.position, player.position) <= seeDistance)
            {
                if (Vector3.Distance(transform.position, player.position) <= stoppingDistance)
                {
                    npc.state = 2;
                    goToBattle.player = GameObject.FindGameObjectWithTag("Player");
                    goToBattle.npc = npc;
                    goToBattle.BattleTime();
                }
                else
                {
                    agent.SetDestination(player.position);
                }
            }

            if (Vector3.Distance(gameObject.transform.position, agent.destination) <= stoppingDistance)
            {
                agent.SetDestination(waypoints[Random.Range(0, waypoints.Length)].position);
            }
        }
        else if (npc.state == 0 || npc.state == 2)
        {
            return;
        }
    }
}
