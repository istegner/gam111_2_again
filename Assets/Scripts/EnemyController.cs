﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public BattleContext battleContext;

    public struct Stats
    {
        public int hp, state;
    }

    public Stats myStats;
}
