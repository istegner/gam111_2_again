﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class NPC : ScriptableObject
{
    [Tooltip("0 - defeated\n1 - active\n2 - attack")]
    public int state;

    public Ability[] abilities;

    // Use this for initialization
    void Start()
    {
        state = 1;
    }
}
