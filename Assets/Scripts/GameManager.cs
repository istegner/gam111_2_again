﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public NPC[] NPCs;
    public GameObject player;

    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }
}
